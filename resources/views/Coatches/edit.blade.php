@extends('layouts.app')

@section('content')
    <div>{{$Coatch}}</div>
    <div class="formWrapper">
        <div class="row DirectionRtl">
            <div class="col-lg-offset-2 col-lg-10">
                <h1 class="page-header">הוספת מאמן חדש</h1>
            </div>
            <!-- /.col-lg-12 -->

        </div>

    <!-- <div>{{$Coatch}}</div> /.row -->
        <div class="row DirectionRtl" >
            <div class="col-lg-offset-2 col-lg-10">
                <div>{{$Coatch}}</div>
                <div class="panel panel-default" >
                    <div class="panel-heading " >
                        עדכן קופון חדש
                    </div>
                    <div class="panel-body ">
                        <div class="row ">
                            <div class="col-lg-12">
                                <form role="form" action="{!! action('CoachController@update', ['id' => $Coatch->id ] ) !!}"  method="post" >
                                    <input name="_method" type="hidden" value="PATCH" >
                                    <div class="form-group">
                                        <input  type="hidden" value="{{$Coatch->id}}" name="id">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>תעודת זהות</label>
                                                <input class="form-control" value="{{$Coatch->identify_number}}" name="identify_number">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>שם המאמן</label>
                                                <input class="form-control" value="{{$Coatch->name}}" name="name">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>אימייל</label>
                                                <input class="form-control" value="{{$Coatch->email}}" name="email">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>טלפון</label>
                                                <input class="form-control" value="{{$Coatch->phone}}" name="phone">
                                            </div>
                                        </div>

                                    </div>



                                    <button type="submit" class="btn btn-default DirectionLtr" style="float:left; width: 120px; text-align: center !important;" >שלח פרטים</button>
                                    {{csrf_field()}}
                                </form>
                            </div>
                            <!-- /.col-lg-6 (nested) -->
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

@endsection






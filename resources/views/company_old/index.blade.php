@extends('layouts.app')

@section('content')

    <section id="content">
        <div class="page page-full page-mail">
            <div class="tbox tbox-sm">

                <!-- ====================================================
                ================= Left Bar ===============================
                ===================================================== -->

                <!-- left side -->
                <div class="tcol w-md bg-tr-white lt b-r">

                    <!-- left side body -->
                    <div class="p-15 collapse collapse-xs collapse-sm" id="mail-nav">

                        <ul class="nav nav-pills nav-stacked nav-sm" id="mail-folders">
                            <li class="active"><a href="mail-inbox.html">Inbox <span class="pull-right badge bg-lightred">6</span></a></li>
                            <li><a href="javascript:;">Sent</a></li>
                            <li><a href="javascript:;">Draft</a></li>
                        </ul>

                        <h5 class="b-b p-10 text-strong">Labels</h5>
                        <ul class="nav nav-pills nav-stacked nav-sm" id="mail-labels">
                            <li><a href="javascript:;"><i class="fa fa-fw fa-circle text-primary"></i>Family</a></li>
                            <li><a href="javascript:;"><i class="fa fa-fw fa-circle text-default"></i>Work</a></li>
                        </ul>

                    </div>
                    <!-- /left side body -->
                </div>
                <!-- /left side -->


                <!-- ====================================================
               ================= Content ===============================
               ===================================================== -->

                <!-- right side -->
                <div class="tcol">
                    @if(count($Companies) == 0)
                        <div class="alert alert-warning alert-dismissable" style="direction:rtl; text-align:right;">
                            <strong>לא נמצאו ספקים , <a href="new_supplier.php"> לחצ/י כאן</a> כדי להוסיף ספק.</strong>
                        </div>
                    @endif
                    <div class="p-15 bg-white b-b">

                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default">צור קטגורייה חדשה</button>
                        </div>
                        <div class="btn-toolbar">
                        </div>
                    </div>

                    <ul class="list-group no-radius no-border" id="mails-list">
                        @foreach($Companies as $item)

                            <li class="list-group-item b-primary" style="direction:rtl; text-align:right;">
                                <div class="media">
                                    <div class="pull-right">
                                        <div class="thumb thumb-sm" style="width:65px;">
                                            <a href=""><img class="img-circle" style="height:65px !important; width:65px !important" border="0" src="https://wordsmith.org/words/images/avatar2_large.png"></a>
                                        </div>
                                    </div>
                                    <div class="media-body">
                                        <div class="media-heading m-0">
                                            <a href="" class="mr-1">{{$item->title}}</a>
                                            <span class="pull-left text-sm text-muted" >
                                                <div class="hidden-xs" style="margin-top:5px;">
                                                    <a href="/coupons/{{$item->id}}" >
                                                        <button  class="btn btn-primary btn-ef btn-ef-7 btn-ef-7b mb-10" style="font-weight:bold; direction:rtl; background-color:#0C3;">
                                                            <span class="badge bg-lightred" style="margin-right:4px;">{{ count($item->coupons) }} </span> מספר קופונים<i class="glyphicon glyphicon-gift" style="padding-top:5px; "></i>
                                                        </button>
                                                    </a>
                                                    <a href="">
                                                        <button  class="btn btn-primary btn-ef btn-ef-7 btn-ef-7b mb-10" style="font-weight:bold; direction:rtl; background-color:#666">ערוך קטגורייה
                                                            <i class="glyphicon glyphicon-edit" style="padding-top:5px;  "></i>
                                                        </button>
                                                    </a>
                                                    {{--<form action=" {{route('company_categories.companies.destroy' , $item->id)}}" method="post">--}}
                                                    <form action="{!! action('CompaniesController@destroy' , ['company_category'=>1, 'company_old'=> $item->id]) !!}" method="post">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <button onClick=""  type="submit" class="btn btn-primary btn-ef btn-ef-7 btn-ef-7b mb-10" style="font-weight:bold; direction:rtl">מחיקת חברה
                                                            <i class="glyphicon glyphicon-trash" style="padding-top:5px; "></i>
                                                        </button>
                                                        {{csrf_field()}}
                                                    </form>

                                                </div>
                                            </span>
                                        </div>
                                        <div>{{$item->title}}</div>
                                    </div>
                                </div>
                            </li>

                        @endforeach
                    </ul>
                    <!-- / mails -->
                    <!-- /right side body -->
                </div>
                <!-- /right side -->
            </div>
        </div>
    </section>
    <!--/ CONTENT -->
@endsection






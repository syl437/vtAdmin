<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompanyCategoriesTableSeeder::class);
       $this->call(CompanySubcategoriesTableSeeder::class);
        $this->call(RegionsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CouponsTableSeeder::class);
        $this->call(MediaSeeder::class);
       /* $this->call(ManagersTableSeeder::class);
        $this->call(PointsTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(PromocodeTableSeeder::class);
        $this->call(CouponUserTableSeeder::class);*/
    }
}

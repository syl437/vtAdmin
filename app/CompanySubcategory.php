<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySubcategory extends Model
{
    public function company()
    {
        return $this->hasMany(Company::class);
    }

    public function company_category()
    {
        return $this->belongsTo(CompanyCategory::class);
    }

}

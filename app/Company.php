<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use Plank\Mediable\Mediable;

class Company extends Model
{
    use Mediable;
    protected $hidden = ['approved','created_at','updated_at'];
    protected $with = ['coupons']; //,'managers'
    //protected $appends = ['phone2'];
    protected $appends = ['logo'];

    public function getLogoAttribute()
    {
        $logo = $this->getMedia('logo');
        if ($logo->count() > 0)
            return $logo[0]->getUrl();
        return '';
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }
/*
    public function company_subcategory()
    {
        return $this->belongsTo(CompanySubcategory::class);
    }



    public function managers()
    {
        return $this->hasMany(Manager::class);
    }


    public function getPhone2Attribute()
    {
        $p = phone($this->phone, ['IL'], PhoneNumberFormat::NATIONAL);
        return $p;
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function (Company $company_old) {
            $company_old->coupons()->delete();
            $company_old->managers()->delete();
        });
    }*/

}

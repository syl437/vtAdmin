<?php

namespace App\Http\Controllers;

use App\Company;
use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Redirect;

class CompanyCouponsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {
        $CompanyName = $company->title;
        $Coupons = $company->coupons;
        $isEdit = true;
        $Delete = true;
        $Description = 'רשימת כל הקופונים של חברת';
        $Url = "coupons";
        $fileds = array('','נושא הקופון','תיאור','מחיר','כמות קיימת');
        $rows = array('id','title','description','price','quantity');
        return view('coupon.index', ['CompanyName' => $CompanyName,'Title' => 'עסקים' ,'Description' => $Description  ,'Categories' => $Coupons , 'fileds' => $fileds , 'rows' => $rows , 'isEdit' => $isEdit , 'Delete' => $Delete ,'url' =>$Url]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {
        return view('coupon.create', ['Company' => $company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo json_encode($request->all()) ;
        $coupon = new Coupon();
        $coupon->company()->associate(Company::find(1));
        $coupon->fill($request->all());
        $coupon->save();

        return redirect('/companies/1/coupons');
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company, $id )
    {
        echo json_encode($company);
        echo($id);
        $Coupons = $company->coupons[$id];
        return view('coupon.edit', ['coupon' => $Coupons, 'Company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, Coupon $coupon)
    {
        echo "shay";
        print_r($request->all());
      //  $coupon->fill($request->all());
      //  $coupon->save();
      //  return redirect('/companies/1/coupons');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "OK";
        //print_r($coupon);
        //$coupon->delete();
        //return redirect('/coupons');
    }
}

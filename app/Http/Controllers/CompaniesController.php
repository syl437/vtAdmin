<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\View\View;
Use Redirect;



class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Company = Company::all();
        //echo json_encode($Company);
        return view('Company.index', ['Company' => $Company]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Coaches = Coaches::all();
        return view('Courses.create', ['Coaches' => $Coaches ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Courses = new Courses();
        $Courses->fill($request->all());
        $Courses->save();
        return redirect('/Courses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Courses = \App\Courses::where('coach_id','=',$id)->get();
        // echo json_encode($Courses);
        if(count($Courses))
            return view('Courses.index', ['Courses' => $Courses]);
        else
            return ("אין תוצאות");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Courses = Courses::Find($id);

        $Coach = $Courses->coach_id;
        $Nutritionist = $Courses->nutritionist_id;

        $Coaches = Coaches::all();

        if(count($Courses))
            return view('Courses.edit', ['Courses' => $Courses, 'Coaches' => $Coaches ]);
        else
            return ("אין תוצאות");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Courses = Courses::Find($request->id);
        $Courses->fill($request->all());
        $Courses->update();
        return redirect('/Courses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Courses = new Courses();
        $Courses = Courses::Find($id);
        $Courses->delete();
        return redirect('/Courses');
    }
}
